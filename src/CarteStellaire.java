import sdbog_mvc.modele.*;
import sdbog_mvc.vue.*;
import sdbog_mvc.controleur.*;

/**
   Projet de Méthodes et Outils pour la Programmation.
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
   @version 4.01.16
 */
public class CarteStellaire
{
    public static void test_creation_system()
    {
	Galaxie galaxie = new Galaxie();
	
	Etoile soleil = new Etoile("Soleil");
	Satellite terre = new Satellite("Terre", soleil);
	Satellite lune = new Satellite("Lune", terre);
	Satellite mars = new Satellite ("Mars", soleil);
	Satellite apollo = new Satellite("Apollo", lune);
	galaxie.ajouterEtoile(soleil);
	galaxie.ajouterEtoile(new Etoile("Osef"));
	galaxie.ajouterEtoile(new Etoile("Kazog"));
	
	System.out.println("------------------------------------------");
	try
	    {
		galaxie.supprimerEtoile("Kazog");
	    }
	catch(Exception e)
	    {
		e.printStackTrace();
	    }
	
	System.out.println(galaxie);	
    }
    
    public static void main(String[] args)
    {
	FenetreControleur fc = new FenetreControleur();
	fc.lancerSimulation();

    }
}
