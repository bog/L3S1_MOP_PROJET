package sdbog_mvc.controleur;
import sdbog_mvc.vue.*;
import sdbog_mvc.modele.*;
import java.awt.event.*;
import java.util.Observable;
import java.io.*;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
   Controle de la fenêtre pricipale.

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
*/
public class FenetreControleur
{
    private Galaxie galaxie;
    private FenetrePrincipale fenetre_principale;
    private FenetreNouvelAstre fenetre_nouvel_astre;
    private FenetreSuppressionAstre fenetre_suppression_astre;
    
    /**
       Constructeur par défaut.
    */
    public FenetreControleur()
    {
	this.galaxie = new Galaxie();
	
	this.fenetre_principale = new FenetrePrincipale();
	this.fenetre_principale.setAjouterAstreEcouteur(new AjouterAstre());
	this.fenetre_principale.setSupprimerAstreEcouteur(new SupprimerAstre());
	this.fenetre_principale.setNouveauSystemeEcouteur(new NouveauSysteme());
	this.fenetre_principale.setEnregistrerSystemeEcouteur(new EnregistrerSysteme());
	this.fenetre_principale.setChargerSystemeEcouteur(new ChargerSysteme());
	
	this.fenetre_nouvel_astre = new FenetreNouvelAstre();
	this.fenetre_nouvel_astre.ajouterAstreTypeEtoileEcouteur(new ChoixAstreEtoile());
	this.fenetre_nouvel_astre.ajouterAstreTypeSatelliteEcouteur(new ChoixAstreSatellite());
	this.fenetre_nouvel_astre.ajouterValiderAjoutAstreEcouteur(new ValiderAjout());
	
	this.fenetre_suppression_astre = new FenetreSuppressionAstre();
	this.fenetre_suppression_astre.ajouterSupprimerAstreEcouteur(new ValiderSuppression());
	
	chargerGalaxie();
    }

    /**
       Permet de charger une nouvelle galaxie en liant
       les différentes fenêtres à la galaxie courante
     */
    public void chargerGalaxie()
    {
	this.fenetre_principale.setObservable(this.galaxie);
	this.galaxie.ajouterObservateurEnsembleEtoile(this.fenetre_nouvel_astre);
	this.galaxie.ajouterObservateurEnsembleEtoile(this.fenetre_suppression_astre);
	this.galaxie.reconstruireObservateurEnsembleEtoile();
    }
    
    /**
       Permet de lancer la simulation et de faire tourner les astres.
    */
    public void lancerSimulation()
    {
	while(true)
	    {
		try
		    {
			Thread.sleep(1);
			this.galaxie.majPositionEtoiles();
			this.galaxie.notifyObservers();
		    }
		catch(Exception exception)
		    {
			exception.printStackTrace();
		    }
	    }
    }
    
    /**
       Action associée à l'ajout d'un astre.
    */
    private class AjouterAstre implements ActionListener
    {
	@Override
	public void actionPerformed(ActionEvent ae)
	{
	    FenetreControleur.this.fenetre_nouvel_astre.setVisible(true);
	}
    }

    /**
       Action associée à la suppression d'un astre.
    */
    private class SupprimerAstre implements ActionListener
    {
	@Override
	public void actionPerformed(ActionEvent ae)
	{
	    FenetreControleur.this.fenetre_suppression_astre.setVisible(true);
	}
    }

    /**
       Action associée à la création d'un système.
    */
    private class NouveauSysteme implements ActionListener
    {
	@Override
	public void actionPerformed(ActionEvent ae)
	{
	    FenetreControleur.this.galaxie = new Galaxie();
	    FenetreControleur.this.chargerGalaxie();
	}

    }
    
    /**
       Action associée à l'enregistrement d'un système.
    */
    private class EnregistrerSysteme implements ActionListener
    {
	@Override
	public void actionPerformed(ActionEvent ae)
	{
	    
	    JFileChooser jfc = new JFileChooser();
	    jfc.showSaveDialog(FenetreControleur.this.fenetre_principale);
	    
	    File file = jfc.getSelectedFile();
	    
	    if(file != null)
		{
		    serialiserGalaxie(file);
		}
	}

	/**
	   Permet de sérialiser la galaxie
	   @param file fichier de sortie de la sérialisation
	 */
	public void serialiserGalaxie(File file)
	{
	    Galaxie galaxie = FenetreControleur.this.galaxie;

	    try
		{
		    FileOutputStream fos = new FileOutputStream(file);
		    ObjectOutputStream oss = new ObjectOutputStream(fos);
		    oss.writeObject(galaxie);
		    oss.flush();
		    oss.close();
		}
	    catch(Exception e)
		{
		    JOptionPane.showMessageDialog(FenetreControleur.this.fenetre_principale
						  , "Échec lors de la sauvegarde de " + file
						  , "Sauvegarde"
						  , JOptionPane.ERROR_MESSAGE);
		    e.printStackTrace();
		}
	}
    }
    
    /**
       Action associée au chargement d'un système.
    */
    private class ChargerSysteme implements ActionListener
    {
	@Override
	public void actionPerformed(ActionEvent ae)
	{

	    JFileChooser jfc = new JFileChooser();
	    jfc.showOpenDialog(FenetreControleur.this.fenetre_principale);
	    
	    File file = jfc.getSelectedFile();
	    
	    if(file != null)
		{
		    deserialiserGalaxie(file);
		}
	}
	
	/**
	   Permet de desérialiser la galaxie
	   @param file fichier de sortie de la desérialisation
	*/
	public void deserialiserGalaxie(File file)
	{
	    try
		{
		    FileInputStream fis = new FileInputStream(file);
		    ObjectInputStream iss = new ObjectInputStream(fis);
		    FenetreControleur.this.galaxie = (Galaxie)( iss.readObject() );
		    FenetreControleur.this.chargerGalaxie();
		    iss.close();

		}
	    catch(Exception e)
		{
		    JOptionPane.showMessageDialog(FenetreControleur.this.fenetre_principale
						  , "Échec lors du chargement de " + file
						  , "Chargement"
						  , JOptionPane.ERROR_MESSAGE);
		    e.printStackTrace();
		}
	}
	
    }

    /**
       Action associée à la selection de l'étoile comme type d'astre à ajouter
    */
    private class ChoixAstreEtoile implements ActionListener
    {
	@Override
	public void actionPerformed(ActionEvent ae)
	{
	    FenetreControleur.this.fenetre_nouvel_astre.afficherFormulaireEtoile();
	}
    }

    /**
       Action associée à la selection du satellite comme type d'astre à ajouter
    */
    private class ChoixAstreSatellite implements ActionListener
    {
	public void actionPerformed(ActionEvent ae)
	{
	    FenetreControleur.this.fenetre_nouvel_astre.afficherFormulaireSatellite();
	}
    }

    /**
       Action associée à la validation de l'ajout d'un astre
    */
    private class ValiderAjout implements ActionListener
    {
	private FenetreNouvelAstre fna;

	/**
	   Constructeur par défaut
	*/
	public ValiderAjout()
	{
	    this.fna = FenetreControleur.this.fenetre_nouvel_astre;
	}
	
	@Override	
	public void actionPerformed(ActionEvent ae)
	{

	    if( this.fna.selectionEtoileUtilisateur() )
		{
		    ajouterEtoile();
		}
	    else
		{
		    ajouterSatellite();
		}
	    
	    this.fna.reinitialiserFormulaires();
	}

	/**
	   Permet de créer un satellite et de l'ajouter à la galaxie.
	*/
	private void ajouterSatellite()
	{

	    String nom = this.fna.getNomAstre();
	    
	    try
		{
		    if( nom.isEmpty()
			|| FenetreControleur.this.galaxie.astreExisteDeja(nom) )
			{
			    if( nom.isEmpty() )
				{
				    nom = "satellite inconnu";
				}

			    throw new AstreNomInvalide();
			}
		    
		    Astre referant = FenetreControleur.this.galaxie.getAstre(this.fna.getAstreReferant());
		    double demi_grand_axe = Double.parseDouble( this.fna.getAstreDemiGrandAxe() );
		    double demi_petit_axe = Double.parseDouble( this.fna.getAstreDemiPetitAxe() );
		    double periode = Double.parseDouble( this.fna.getAstrePeriode() );

		    Satellite nouveau_satellite = new Satellite(this.fna.getNomAstre()
								, referant
								, periode
								, demi_petit_axe
								, demi_grand_axe);
		}
	    catch(Exception e)
		{
		    JOptionPane.showMessageDialog(FenetreControleur.this.fenetre_nouvel_astre
						  , "Échec lors de l'ajout de "
						  + "\"" + nom + "\""
						  , "Ajout d'un satellite"
						  , JOptionPane.ERROR_MESSAGE);

		    e.printStackTrace();
		}
	}

	/**
	   Permet de créer une étoile et de l'ajouter à la galaxie.
	*/
	private void ajouterEtoile()
	{
	    
	    String nom = this.fna.getNomAstre();
	    
	    try
		{
		    if( nom.isEmpty()
			|| FenetreControleur.this.galaxie.astreExisteDeja(nom) )
			{
			    if( nom.isEmpty() )
				{
				    nom = "étoile inconnue";
				}

			    throw new AstreNomInvalide();
			}
		     
		    double x = Double.parseDouble( this.fna.getAstreX() );
		    double y = Double.parseDouble( this.fna.getAstreY() );
		    Position pos = new Position(x, y);
		    FenetreControleur.this.galaxie.ajouterEtoile(new Etoile(nom, pos));
		}
	    catch(Exception e)
		{
		    JOptionPane.showMessageDialog(FenetreControleur.this.fenetre_nouvel_astre
						  , "Échec lors de l'ajout de \"" + nom + "\""
						  , "Ajout d'une étoile"
						  , JOptionPane.ERROR_MESSAGE);
				    
		    e.printStackTrace();
		}
	}
    }

    /**
       Action associée à la suppression d'un astre
    */
    private class ValiderSuppression implements ActionListener
    {
	private FenetreSuppressionAstre fsa;
	
	/**
	   Constructeur par défaut
	*/
	public ValiderSuppression()
	{
	    this.fsa = FenetreControleur.this.fenetre_suppression_astre;
	}
	
	@Override
	public void actionPerformed(ActionEvent ae)
	{
	    String nom_astre = this.fsa.getAstreNom();
	    Galaxie galaxie = FenetreControleur.this.galaxie;
	    

	    try
		{
		    if( nom_astre.isEmpty() )
			{
			    nom_astre = "astre inconnu";
			    throw new AstreNomInvalide();
			}
		    
		    galaxie.supprimerAstre(nom_astre);
		}
	    catch(Exception e)
		{
		    JOptionPane.showMessageDialog(FenetreControleur.this.fenetre_suppression_astre
						  , "Échec lors de la suppression de \""
						  + nom_astre + "\""
						  , "Suppression d'un astre"
						  , JOptionPane.ERROR_MESSAGE);
		    e.printStackTrace();
		}
	}
    }
}
