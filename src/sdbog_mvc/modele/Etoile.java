package sdbog_mvc.modele;

/**
   Astre fixe.

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class Etoile extends Astre
{
/**
       Construit un etoile nommé à une position donnée.
       @param nom nom de l'etoile
       @param position position du centre de l'etoile
    */
    public Etoile(String nom, Position position)
    {
	super(nom, position);
    }

    /**
       Construit un etoile à une position donnée.
       @param position position du centre de l'etoile
    */
    public Etoile(Position position)
    {
	super(position);
    }

    /**
       Construit un etoile avec un nom donné.
       @param nom nom de l'etoile
    */
    public Etoile(String nom)
    {
	super(nom);
    }
    
    /**
       Constructeur par défaut.
    */
    public Etoile()
    {
	super();
    }
}
