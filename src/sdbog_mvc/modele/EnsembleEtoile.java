package sdbog_mvc.modele;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Collections;
import java.util.Comparator;
import java.io.Serializable;

/**
   Ensemble d'étoiles

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class EnsembleEtoile extends Observable implements Observer, Serializable
{
    private HashMap<String, Etoile> etoiles;
    private ArrayList<String> nom_etoiles;

    public EnsembleEtoile()
    {
	this.etoiles = new HashMap<String, Etoile>();
	this.nom_etoiles = new ArrayList<String>();
	setChanged();
    }

    /**
       Permet d'ajouter une étoile à l'ensemble des étoiles.
       @param etoile étoile à ajouter à l'ensemble des étoiles
    */
    public void ajouterEtoile(Etoile etoile)
    {
	this.etoiles.put(etoile.getNom(), etoile);
	this.nom_etoiles.add(etoile.getNom());
	etoile.addObserver(this);
	setChanged();
    }

    /**
       Permet de supprimer une étoile de l'ensemble des étoiles
       @param etoile étoile à supprimer de l'ensemble des étoiles
       @throws EtoileIntrouvable si l'étoile n'est pas présente dans l'ensemble des étoiles
    */
    public void supprimerEtoile(Etoile etoile)  throws EtoileIntrouvable
    {
	if( this.etoiles.get( etoile.getNom() ) == null )
	    {
		throw new EtoileIntrouvable();
	    }
	
	this.etoiles.remove(etoile);
	this.nom_etoiles.remove(etoile.getNom());
	setChanged();
    }

    /**
       Permet de supprimer une étoile de l'ensemble des étoiles à partir de son nom
       @param nom nom de l'étoile à supprimer de l'ensemble des étoiles
       @throws EtoileIntrouvable si l'étoile n'est pas présente dans l'ensemble des étoiles
    */
    public void supprimerEtoile(String nom) throws EtoileIntrouvable
    {
	try
	    {
		supprimerEtoile(getEtoile(nom));
	    }
	catch(EtoileIntrouvable exception)
	    {
		throw exception;
	    }
	
	setChanged();
    }

    /**
       Permet de supprimer n'importe quel astre de l'ensemble des étoiles à partir de son nom
       @param nom nom de l'astre à supprimer
       @throws AstreIntrouvable si l'astre à supprimer n'est pas dans l'ensemble des étoiles
     */
    public void supprimerAstre(String nom) throws AstreIntrouvable
    {
	try
	    {
		Astre astre = getAstre(nom);

		try
		    {
			Etoile etoile = (Etoile)(astre);
			supprimerEtoile(etoile);

		    }
		catch(Exception exception)
		    {
			Satellite satellite = (Satellite)(astre);
			try
			    {
				satellite.getAstre().supprimerSatellite(satellite);
			    }
			catch(SatelliteIntrouvable satellite_introuvable)
			    {
				throw new AstreIntrouvable();
			    }
		    }
		
		setChanged();
	    }
	catch(AstreIntrouvable astre_introuvable)
	    {
		throw astre_introuvable;
	    }
	
    }
    
    /**
       Permet d'obtenir une étoile de la galaxie à partir de son nom.
       @param nom nom de l'étoile à récuperer
       @return resultat étoile correspondant au nom
       @throws EtoileIntrouvable si l'étoile n'est pas dans l'ensemble des étoiles
     */
    public Etoile getEtoile(String nom) throws EtoileIntrouvable
    {
	Etoile resultat = this.etoiles.get(nom);
	
	if(resultat == null)
	    {
		throw new EtoileIntrouvable();
	    }
	
	return resultat;
    }

     /**
       Permet d'obtenir une liste de tous les astres de l'ensemble des étoiles
       triés par profondeur.
       
       @return res liste contenant tous les astres de l'ensemble des étoiles.
     */
    public ArrayList<Astre> getTousLesAstres()
    {
    	ArrayList<Astre> res = new ArrayList<Astre>();

    	for(String nom : this.nom_etoiles)
    	    {
    		ArrayList<Satellite> satellites;
		satellites = this.etoiles.get(nom).getTousLesSatellites();

    		res.add( this.etoiles.get(nom) );
		
    		for(Satellite satellite : satellites)
    		    {
    			res.add(satellite);
    		    }
    	    }
	
    	return res;
    }

    /**
       Permet d'obtenir une liste de tous les astres de l'ensemble des étoiles
       triés par nom.
       
       @return res liste contenant tous les astres de l'ensemble des étoiles.
     */
    public ArrayList<Astre> getTousLesAstresTrie()
    {
    	ArrayList<Astre> res = getTousLesAstres();
	
    	Collections.sort(res, new Comparator<Astre>(){
		public int compare(Astre a, Astre b)
		{
		    return a.getNom().compareTo( b.getNom() );
		}
	    });
	
    	return res;
    }

    /**
       Permet d'obtenir n'importe quel astre de la galaxie à partir de son nom.
       @param nom nom de l'astre à récupérer
       @return resultat astre correspondant au nom
       @throws AstreIntrouvable si l'astre n'est pas dans la galaxie
     */
    public Astre getAstre(String nom) throws AstreIntrouvable
    {
	ArrayList<Astre> tous_les_astres = getTousLesAstres();
	
	for(Astre astre : tous_les_astres)
	    {
		if( astre.getNom().equals(nom) )
		    {
			return astre;
		    }
	    }

	throw new AstreIntrouvable();
    }
    
    /**
       Met-à-jour la position de toutes les étoiles de l'ensemble des étoiles
       @param temps temps de réference pour la position des astres
    */
    public void majPositionEtoiles(int temps)
    {
	for(String nom : this.nom_etoiles)
	    {
		this.etoiles.get(nom).majPositionSatellites(temps);
	    }
    }
    
    /**
       Permet d'obtenir le nom de toutes les étoiles de l'ensemble des étoiles
       @return this.nom_etoile une liste contenant les noms
     */
    public ArrayList<String> getNoms()
    {
	return this.nom_etoiles;
    }

    /**
       Permet de reporter l'ajout d'un satellite dans la galaxie afin
       de spécifier que l'ensemble des étoiles à changé.
       @param observable Astre ayant ajouté un satellite (non utilisé)
       @param satellite Satellite ayant été ajouté par un Astre
     */
    @Override
    public void update(Observable observable, Object satellite)
    {
	if( satellite != null )
	    {
		Astre astre = (Astre)(satellite);
		astre.addObserver(this);
	    }

	setChanged();
    }
    
}
