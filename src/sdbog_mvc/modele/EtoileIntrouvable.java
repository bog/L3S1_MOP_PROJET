package sdbog_mvc.modele;

public class EtoileIntrouvable extends Exception
{
    public EtoileIntrouvable()
    {
	super("L'étoile n'est pas présente dans la galaxie");
    }
}
