package sdbog_mvc.modele;

/**
   Exception lancée lorsque un satellite n'est pas trouvé dans une collection
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class SatelliteIntrouvable extends Exception
{
    public SatelliteIntrouvable()
    {
	super("Le satellite n'est pas présent dans la collection");
    }
}
