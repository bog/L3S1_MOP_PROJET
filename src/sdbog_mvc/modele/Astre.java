package sdbog_mvc.modele;
import java.util.ArrayList;
import java.util.Observable;
import java.io.Serializable;

/**
   Représentation d'un objet celeste.

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
   @see EnsembleEtoile
*/

public abstract class Astre extends Observable implements Serializable
{
    protected String nom;
    protected Position position;
    protected ArrayList<Satellite> mes_satellites;

    /**
       Construit un astre nommé à une position donnée.
       @param nom nom de l'astre
       @param position position du centre de l'astre
    */
    public Astre(String nom, Position position)
    {
	this.mes_satellites = new ArrayList<Satellite>();
	this.nom = nom;
	this.position = position;
	
	setChanged();	
    }
    
    /**
       Construit un astre à une position donnée.
       @param position position du centre de l'astre
    */
    public Astre(Position position)
    {
	this("Inconnu", position);
    }

    /**
       Construit un astre avec un nom donné.
       @param nom nom de l'astre
    */
    public Astre(String nom)
    {
	this(nom, new Position());
    }
    
    /**
       Constructeur par défaut.
    */
    public Astre()
    {
	this("Inconnu", new Position());
    }

    /**
       Permet d'obtenir le nom de l'astre.
       @return nom nom de l'astre
    */
    public String getNom()
    {
	return this.nom;
    }
    
    /**
       Permet d'obtenir la position de l'astre.
       @return position position de l'astre
    */
    public Position getPosition()
    {
	return this.position;
    }

    /**
       Permet de modifier la position de l'astre.
       @param position nouvelle position de l'astre
    */
    public void setPosition(Position position)
    {
	this.position = position;
    }

    /**
       Ajoute un satellite autour de cet astre.
       @param satellite satellite à ajouter
       @throws NullPointerException si satellite est nul
    */
    public void ajouterSatellite(Satellite satellite) throws NullPointerException
    {
	if( satellite == null ){ throw new NullPointerException(); }
	this.mes_satellites.add(satellite);
	
	setChanged();
	notifyObservers(satellite);
    }

    /**
       Supprime un satellite autour de cet astre.
       @param satellite satellite à supprimer
       @throws SatelliteIntrouvable si satellite n'est pas un satellite de cet astre
     */
    public void supprimerSatellite(Satellite satellite) throws SatelliteIntrouvable
    {
	int indice = this.mes_satellites.indexOf(satellite);

	if( indice == -1 )
	    {
		throw new SatelliteIntrouvable();
	    }
	
	satellite.supprimerSatellites();
	this.mes_satellites.remove(indice);

	setChanged();
	notifyObservers();
    }
    
    /**
       Supprime tous les satellites autour de cet astre.
     */
    public void supprimerSatellites()
    {
	this.mes_satellites.clear();
	setChanged();
	notifyObservers();
    }

    /**
       Convertit un astre en chaine de caractère.
       @return une chaine de caractère contenant le nom, la position et les satellites de cet astre
    */
    public String toString()
    {
	String chaine = this.nom + " " + this.position;

	if( this.mes_satellites.size() == 0 )
	    {
		return chaine;
	    }

	for(Satellite satellite : this.mes_satellites)
	    {
		chaine += "\n\t" + satellite;
	    }
	
	return chaine;
    }

    /**
       Permet d'obtenir la liste des satellites tournant autour de cet astre.
       @return mes_satellites la liste des satellites associés à cet astre
     */
    public ArrayList<Satellite> getSatellites()
    {
	return this.mes_satellites;
    }

    /**
       Permet d'obtenir tous les satellites du système formé par cet astre.
       @param tous_les_satellites liste des satellites associés au système formé par cet astre
     */
    public void getTousLesSatellites(ArrayList<Satellite> tous_les_satellites)
    {
	for(Satellite s : this.mes_satellites)
	    {
		tous_les_satellites.add(s);
		s.getTousLesSatellites( tous_les_satellites );
	    }
    }

    /**
       Permet d'obtenir tous les satellites du système formé par cet astre.
       @return tous_les_satellites liste des satellites associés au système formé par cet astre
    */
    public ArrayList<Satellite> getTousLesSatellites()
    {
	ArrayList<Satellite> tous_les_satellites;
	tous_les_satellites = new ArrayList<Satellite>();

	getTousLesSatellites(tous_les_satellites);
	return tous_les_satellites;
    }

    /**
       Met-à-jour la position de tous les satellites d'un astre
       @param temps temps de réference pour la position de l'astre
     */
    public void majPositionSatellites(int temps)
    {
	for(Satellite satellite : this.mes_satellites)
	    {
		satellite.majPosition(temps);
	    }
    }
    
}
