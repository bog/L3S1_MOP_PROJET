package sdbog_mvc.modele;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Collections;
import java.util.Comparator;
import java.io.Serializable;

/**
   Ensemble d'étoiles et leur système associé.

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class Galaxie extends Observable implements Serializable
{
    private int temps;
    private EnsembleEtoile etoiles;
    
    /**
       Constructeur par défaut.
     */
    public Galaxie()
    {
	this.temps = 0;
	this.etoiles = new EnsembleEtoile();
	setChanged();
    }

    /**
       Permet d'ajouter une étoile à la Galaxie.
       @param etoile étoile à ajouter à la galaxie
     */
    public void ajouterEtoile(Etoile etoile)
    {
	this.etoiles.ajouterEtoile(etoile);
	setChanged();
    }

    /**
       Permet de supprimer une étoile de la Galaxie.
       @param etoile étoile à supprimer de la galaxie
       @throws EtoileIntrouvable si l'étoile n'est pas présente dans la galaxie
    */
    public void supprimerEtoile(Etoile etoile)  throws EtoileIntrouvable
    {
	this.etoiles.supprimerEtoile(etoile);
	setChanged();
    }

    /**
       Permet de supprimer une étoile de la Galaxie à partir de son nom
       @param nom nom de l'étoile à supprimer de la galaxie
       @throws EtoileIntrouvable si l'étoile n'est pas présente dans la galaxie
    */
    public void supprimerEtoile(String nom) throws EtoileIntrouvable
    {
	this.etoiles.supprimerEtoile(nom);
	setChanged();
    }

    /**
       Permet de supprimer n'importe quel astre de la galaxie à partir de son nom
       @param nom nom de l'astre à supprimer
       @throws AstreIntrouvable si l'astre à supprimer n'est pas dans la galaxie
     */
    public void supprimerAstre(String nom) throws AstreIntrouvable
    {
	this.etoiles.supprimerAstre(nom);
	setChanged();
    }
    
    /**
       Permet d'obtenir une étoile de la galaxie à partir de son nom.
       @param nom nom de l'étoile à récupérer
       @return resultat étoile correspondant au nom
       @throws EtoileIntrouvable si l'étoile n'est pas dans la galaxie
     */
    public Etoile getEtoile(String nom) throws EtoileIntrouvable
    {
	return this.etoiles.getEtoile(nom);
    }

    /**
       Permet d'obtenir n'importe quel astre de la galaxie à partir de son nom.
       @param nom nom de l'astre à récupérer
       @return resultat astre correspondant au nom
       @throws AstreIntrouvable si l'astre n'est pas dans la galaxie
     */
    public Astre getAstre(String nom) throws AstreIntrouvable
    {
	return this.etoiles.getAstre(nom);
    }

    /**
       Permet de savoir si un astre existe ou non dans la galaxie
       @param nom nom de l'astre à rechercher
       @return true si l'astre est présent dans la galaxie, false sinon
     */
    public boolean astreExisteDeja(String nom)
    {
	try
	    {
		getAstre(nom);
		return true;
	    }
	catch(Exception e)
	    {
		return false;
	    }
    }
    
    /**
       Convertit la galaxie en chaine de caractère.
       @return res la chaine de caractère correspondant à cette galaxie
     */
    public String toString()
    {
	String res = "Galaxie\n";
	
	for(String nom : this.etoiles.getNoms())
	    {
		try
		    {
			res += this.etoiles.getEtoile(nom).toString();
		    }
		catch(Exception exception)
		    {
			res += "étoile inconnue";
		    }
		
		res += "\n";
	    }
	
	return res;
    }

    /**
       Permet d'obtenir une liste de tous les astres de la galaxie
       triés par profondeur.
       
       @return res liste contenant tous les astres de la galaxie.
     */
    public ArrayList<Astre> getTousLesAstres()
    {
	return this.etoiles.getTousLesAstres();
    }

    /**
       Permet d'obtenir une liste de tous les astres de la galaxie
       triés par nom.
       
       @return res liste contenant tous les astres de la galaxie.
     */
    public ArrayList<Astre> getTousLesAstresTrie()
    {
    	return this.etoiles.getTousLesAstresTrie();
    }

    /**
       Met-à-jour la position de toutes les étoiles de la galaxie
    */
    public void majPositionEtoiles()
    {
	this.etoiles.majPositionEtoiles(this.temps);
	this.temps++;
	setChanged();
    }

    /**
       Permet d'ajouter un observateur à l'ensemble des étoiles.
       Le nouvel observateur sera ainsi uniquement notifié par les changements
       concernant la liste des étoiles.
       
       @param observer nouvel observateur de l'ensemble des étoiles
     */
    public void ajouterObservateurEnsembleEtoile(Observer observer)
    {
	this.etoiles.addObserver(observer);
	setChanged();
	
	this.etoiles.update(null, null);
	this.etoiles.notifyObservers();
    }

    /**
       Permet d'obtenir l'ensemble de étoiles de la galaxie
       @return this.etoiles l'ensemble de étoiles de la galaxie
     */
    public EnsembleEtoile getEnsembleEtoile()
    {
	return this.etoiles;
    }
    
    /**
       Permet de mettre-à-jour tous les observateurs de la galaxie.
     */
    @Override
    public void notifyObservers()
    {
	super.notifyObservers();
	this.etoiles.notifyObservers();
    }

    /**
       Permet de reconstruire la galaxie en permettant à l'ensemble des étoiles
       d'observer toutes les étoiles.
     */
    public void reconstruireObservateurEnsembleEtoile()
    {
	ArrayList<Astre> astres = getTousLesAstres();

	for(Astre astre : astres)
	    {
		astre.addObserver(this.etoiles);
	    }
    }
}
