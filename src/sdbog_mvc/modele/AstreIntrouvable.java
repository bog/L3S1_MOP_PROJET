package sdbog_mvc.modele;

/**
   Exception lancée lorsque un astre n'a pas été trouvé dans une collection
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class AstreIntrouvable extends Exception
{
    public AstreIntrouvable()
    {
	super("L'astre n'est pas présent dans la galaxie");
    }
}
