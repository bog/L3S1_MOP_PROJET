package sdbog_mvc.modele;

/**
   Exception lancée lorsque le nom d'un astre n'est pas accepté par l'application.

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
*/
public class AstreNomInvalide extends Exception
{
    public AstreNomInvalide()
    {
	super("Un astre doit posséder un nom");
    }
}
