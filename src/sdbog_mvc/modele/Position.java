package sdbog_mvc.modele;
import java.io.Serializable;
/**
   Coordonnées d'un point dans un plan.
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
   
*/
public class Position implements Serializable
{
    private double x;
    private double y;

    /**
       Constructeur par défaut
     */
    public Position()
    {
	this(0, 0);
    }

    /**
       Construit un point à partir de ses coordonnées.
       @param x coordonnée sur l'axe des abscisses
       @param y coordonnée sur l'axe des ordonnées
     */
    public Position(double x, double y)
    {
	this.x = x;
	this.y = y;
    }

    /**
       Convertit une position en chaine de caractère.
       @return une chaine de caractère contenant la position au format (x, y)
     */
    public String toString()
    {
	return "(" + this.x + ", " + this.y +")";
    }

    /**
       Permet d'obtenir la coordonnée en x de la position.
       @return x
     */
    public double getX()
    {
	return this.x;
    }

     /**
       Permet d'obtenir la coordonnée en y de la position.
       @return y
     */
    public double getY()
    {
	return this.y;
    }

    /**
       Permet de modifier la coordonnée en x de la position.
       @param x la nouvelle valeur de x
     */
    public void setX(double x)
    {
	this.x = x;
    }

    /**
       Permet de modifier la coordonnée en y de la position.
       @param y la nouvelle valeur de y
    */
    public void setY(double y)
    {
	this.y = y;
    }

    /**
       Permet de modifier les coordonnées de la position.
       @param x la nouvelle valeur de x
       @param y la nouvelle valeur de y
    */
    public void setXY(double x, double y)
    {
	setX(x);
	setY(y);
    }

    /**
       Déplace un point dans le plan.
       @param x déplacement relatif en x
       @param y déplacement relatif en y
     */
    public void move(double x, double y)
    {
	this.x += x;
	this.y += y;
    }
}
