package sdbog_mvc.modele;

/**
   Astre tournant autour d'un autre astre.
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
   @see Etoile
 */
public class Satellite extends Astre
{
    private Astre mon_astre;
    private double periode;
    private double demi_petit_axe;
    private double demi_grand_axe;
    
    /**
       Construit un satellite nommé autour d'un astre.
       @param nom nom du satellite
       @param astre astre autour duquel tourne ce satellite
     **/
    public Satellite(String nom, Astre astre)
    {
	this(nom, astre, 100.0, 150, 200);
    }
    
    /**
       Construit un satellite nommé paramétré.
       @param nom nom du satellite
       @param astre astre autour duquel tourne ce satellite
       @param periode période de rotation du satellite autour de son astre (en seconde)
       @param demi_petit_axe longueur du demi_petit_axe de l'ellipse décrite par le satellite
       @param demi_grand_axe longueur du demi_grand_axe de l'ellipse décrite par le satellite
     */
    public Satellite(String nom, Astre astre, double periode, double demi_petit_axe, double demi_grand_axe)
    {
	super(nom);
	
	this.mon_astre = astre;
	this.mon_astre.ajouterSatellite(this);
	
	this.periode = periode;
	
	this.demi_petit_axe = demi_petit_axe;
	this.demi_grand_axe = demi_grand_axe;
    }

    /**
       Construit un satellite paramétré.
       @param astre astre autour duquel tourne ce satellite
       @param periode période de rotation du satellite autour de son astre (en seconde)
       @param demi_petit_axe longueur du demi_petit_axe de l'ellipse décrite par le satellite
       @param demi_grand_axe longueur du demi_grand_axe de l'ellipse décrite par le satellite
     */
    public Satellite(Astre astre, double periode, double demi_petit_axe, double demi_grand_axe)
    {
	this("Satellite inconnu", astre, periode, demi_petit_axe, demi_grand_axe);
    }
    
    /**
       Met-à-jour la position du satellite en fonction du temps de
       telle sorte qu'il tourne autour de son astre référant.
       @param temps temps absolue en millisecondes
     */
    public void majPosition(int temps)
    {
	this.position.setX( this.demi_grand_axe *  Math.cos(temps/this.periode)
			    + this.mon_astre.getPosition().getX() );

	this.position.setY( this.demi_petit_axe *  Math.sin(temps/this.periode)
			    + this.mon_astre.getPosition().getY() );
	
	majPositionSatellites(temps);
    }

    /**
       Permet d'obtenir l'astre référant du satellite
       @return this.mon_astre l'astre référant du satellite
     */
    public Astre getAstre()
    {
	return this.mon_astre;
    }

    /**
       Permet d'obtenir la profondeur d'un satellite par rapport à l'étoile autour de laquelle
       elle tourne.

       @return profondeur la profondeur du satellite
     */
    public int getProfondeur()
    {
	int profondeur = 1;
	
	try
	    {
		Satellite referant = (Satellite)(this.mon_astre);
		
		while(true)
		    {
			profondeur++;
			referant = (Satellite)(referant.getAstre());
		    }
	    }
	catch(Exception exception)
	    {
		return profondeur;
	    }
    }
}
