package sdbog_mvc.vue;

/**
   Exception lancée lorsque une ressource est déjà présente en mémoire
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class RessourceDejaExistante extends Exception
{
    public RessourceDejaExistante()
    {
	super("La ressource existe déjà en mémoire");
    }
}
