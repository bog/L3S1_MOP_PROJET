package sdbog_mvc.vue;

/**
   Exception lancée lorsque une ressource demandée n'a pu être fournie
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class RessourceIntrouvable extends Exception
{
    public RessourceIntrouvable()
    {
	super("La ressource demandée est introuvable");
    }
}
