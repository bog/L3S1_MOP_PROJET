package sdbog_mvc.vue;
import java.awt.*;
import javax.swing.*;

/**
   Formulaire permettant la saisie des caractéristiques d'un satellite.
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
*/
public class FormulaireAjoutSatellite extends Formulaire
{
    private Choice choix_astre;
    private JTextField txt_demi_grand_axe;
    private JTextField txt_demi_petit_axe;
    private JTextField txt_periode;
    
    /**
       Constructeur par défaut.
    */  
    public FormulaireAjoutSatellite()
    {
	super();

	this.setLayout(new GridLayout(0, 2));

	this.add(new JLabel("Astre référant : ", SwingConstants.RIGHT));
	this.choix_astre = new Choice();
	this.add(this.choix_astre);

	this.add(new JLabel("Demi grand axe : ", SwingConstants.RIGHT));
	this.txt_demi_grand_axe = new JTextField("");
	this.add(this.txt_demi_grand_axe);

	this.add(new JLabel("Demi petit axe : ", SwingConstants.RIGHT));
	this.txt_demi_petit_axe = new JTextField("");
	this.add(this.txt_demi_petit_axe);

	this.add(new JLabel("Période : ", SwingConstants.RIGHT));
	this.txt_periode = new JTextField("");
	this.add(this.txt_periode);
    }

    /**
       Permet d'ajouter un astre dans la liste du choix des astres
       @param nom astres à ajouter
     */
    public void ajouterAstre(String nom)
    {
	this.choix_astre.add(nom);
    }

    /**
       Permet de réinitialiser la liste du choix des astres
     */
    public void reinitialiserAstres()
    {
	this.choix_astre.removeAll();
    }

    /**
       Permet d'obtenir une chaine représentant l'astre autour duquel tourne le satellite saisie
       @return astre le nom de l'astre référant
    */
    public String getAstreReferant()
    {
	String astre = this.choix_astre.getSelectedItem();
	return astre;
    }

    /**
       Permet d'obtenir une chaine représentant le demi grand axe du satellite saisie
       @return demi_grand_axe le demi grand axe du satellite en question
     */
    public String getAstreDemiGrandAxe()
    {
	String demi_grand_axe = this.txt_demi_grand_axe.getText();
	return demi_grand_axe;
    }

    /**
       Permet d'obtenir une chaine représentant le demi petit axe du satellite saisie
       @return demi_petit_axe le demi petit axe du satellite en question
     */
    public String getAstreDemiPetitAxe()
    {
	String demi_petit_axe = this.txt_demi_petit_axe.getText();
	return demi_petit_axe;
    }

    
    /**
       Permet d'obtenir une chaine représentant la période du satellite saisie
       @return periode la période du satellite en question
     */
    public String getAstrePeriode()
    {
	String periode = this.txt_periode.getText();
	return periode;
    }

    /**
       Permet d'effacer les saisies effectuées par l'utilisateur
    */
    public void reinitialiserFormulaire()
    {
	if(this.choix_astre.getItemCount() > 0)
	    {
		this.choix_astre.select(0);
	    }
	
	this.txt_demi_grand_axe.setText("");
	this.txt_demi_petit_axe.setText("");
	this.txt_periode.setText("");

    }
}
