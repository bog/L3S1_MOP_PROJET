package sdbog_mvc.vue;
import java.awt.*;
import javax.swing.*;

/**
   Formulaire permettant la saisie des caractéristiques d'une étoile.
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
*/
public class FormulaireAjoutEtoile extends Formulaire
{
    private JTextField txt_position_x;
    private JTextField txt_position_y;

   
    /**
       Constructeur par défaut.
     */
    public FormulaireAjoutEtoile()
    {
	super();

	this.setLayout(new GridLayout(0, 2));
	
	this.add(new JLabel("Position horizontale de l'astre : ", SwingConstants.RIGHT));
	this.txt_position_x = new JTextField();
	this.add(this.txt_position_x);

	this.add(new JLabel("Position verticale de l'astre : ", SwingConstants.RIGHT));
	this.txt_position_y = new JTextField();
	this.add(this.txt_position_y);

    }

    /**
       Permet d'obtenir  une chaine représentant la position en X de l'étoile saisie
       @return pos_x la position en X de l'astre en question
    */
    public String getAstreX()
    {
	String pos_x = this.txt_position_x.getText();
	return pos_x;
    }

    /**
       Permet d'obtenir une chaine représentant la position en Y de l'étoile saisie
       @return pos_y la position en Y de l'astre en question
     */
    public String getAstreY()
    {
	String pos_y = this.txt_position_y.getText();
	return pos_y;
    }

    /**
       Permet d'effacer les saisies effectuées par l'utilisateur
    */
    public void reinitialiserFormulaire()
    {
	this.txt_position_x.setText("");
	this.txt_position_y.setText("");
    }
}
