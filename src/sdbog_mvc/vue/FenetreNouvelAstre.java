package sdbog_mvc.vue;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import javax.swing.*;
import java.util.*;
import sdbog_mvc.modele.Galaxie;
import sdbog_mvc.modele.EnsembleEtoile;
import sdbog_mvc.modele.Astre;

/**
   Fenetre permettant l'ajout d'un astre.

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class FenetreNouvelAstre extends JFrame implements Observer
{
    static final int LARGEUR = 600;
    static final int HAUTEUR = 320;
    static final String TITLE = "Nouvel astre";
    
    private FormulaireAjoutAstre formulaire_astre;
    private FormulaireAjoutEtoile formulaire_etoile;
    private FormulaireAjoutSatellite formulaire_satellite;

    private JButton bouton_valider;
    
    /**
       Constructeur par défaut.
     */
    public FenetreNouvelAstre()
    {
	this.getContentPane().setLayout(new GridLayout(0, 1));
	
	this.formulaire_astre = new FormulaireAjoutAstre();	
	this.formulaire_etoile = new FormulaireAjoutEtoile();
	this.formulaire_satellite = new FormulaireAjoutSatellite();
	this.bouton_valider = new JButton("Ajouter l'astre");
	
	afficherFormulaireEtoile();
	
	this.setSize(FenetreNouvelAstre.LARGEUR, FenetreNouvelAstre.HAUTEUR);
	this.setLocationRelativeTo(null);
	this.setTitle(FenetreNouvelAstre.TITLE);
    }

    /**
       Permet de mettre à jour la liste des astres présent dans un ensemble d'étoile.
       @param observable ensemble d'étoile observée par la fenetre
       @param object non utilisé
     */
    @Override
    public void update(Observable observable, Object object)
    {
	this.formulaire_satellite.reinitialiserAstres();
	EnsembleEtoile etoiles = (EnsembleEtoile)(observable);
	ArrayList<Astre> astres = etoiles.getTousLesAstresTrie();

	for(Astre astre : astres)
	    {
		this.formulaire_satellite.ajouterAstre(astre.getNom());
	    }
	
	if( astres.size() == 0 )
	    {
		this.formulaire_astre.desactiverSatellite();
	    }
	else
	    {
		this.formulaire_astre.activerSatellite();
	    }
    }

    /**
       Permet de lier le choix de l'étoile comme type de l'astre à un écouteur.

       @param ecouteur écouteur chargé d'écouter la selection du radio bouton étoile.
    */
    public void ajouterAstreTypeEtoileEcouteur(ActionListener ecouteur)
    {
	this.formulaire_astre.ajouterAstreTypeEtoileEcouteur(ecouteur);
    }

    /**
       Permet de lier le choix du satellite comme type de l'astre à un écouteur.

       @param ecouteur écouteur chargé d'écouter la selection du radio bouton satellite.
    */
    public void ajouterAstreTypeSatelliteEcouteur(ActionListener ecouteur)
    {
	this.formulaire_astre.ajouterAstreTypeSatelliteEcouteur(ecouteur);
    }

    /**
       Permet de lier la validation de l'astre à ajouter à un écouteur.

       @param ecouteur écouteur chargé d'écouter la validation d'un astre
    */
    public void ajouterValiderAjoutAstreEcouteur(ActionListener ecouteur)
    {
	this.bouton_valider.addActionListener(ecouteur);
    }
    
    /**
       Permet d'afficher le formulaire permettant l'ajout d'une étoile et masque la partie 
       du formulaire correspondant à l'ajout d'un satellite
     */
    public void afficherFormulaireEtoile()
    {
	this.getContentPane().removeAll();
	this.getContentPane().add(this.formulaire_astre);	    
	this.getContentPane().add(this.formulaire_etoile);
	this.getContentPane().add(this.bouton_valider);
	this.getContentPane().validate();
	repaint();
    }

    /**
       Permet d'afficher le formulaire permettant l'ajout d'un satellite et masque la partie 
       du formulaire correspondant à l'ajout d'une étoile
    */
    public void afficherFormulaireSatellite()
    {
	this.getContentPane().removeAll();
	this.getContentPane().add(this.formulaire_astre);	    
	this.getContentPane().add(this.formulaire_satellite);
	this.getContentPane().add(this.bouton_valider);
	this.getContentPane().validate();
	repaint();
    }

    /**
       Permet de savoir si l'utilisateur à selectionné la case étoile comme type d'astre.
       @return est_une_etoile si la case étoile est selectionnée dans l'interface
       d'ajout d'un astre
     */
    public boolean selectionEtoileUtilisateur()
    {
	boolean est_une_etoile = this.formulaire_astre.selectionEtoileUtilisateur();
	return est_une_etoile;
    }

    /**
       Permet d'obtenir le nom de l'astre saisie
       @return nom_astre le nom de l'astre en question
     */
    public String getNomAstre()
    {
	String nom_astre = this.formulaire_astre.getNomAstre();
	return nom_astre;
    }

    /**
       Permet d'obtenir une chaine représentant  la position en X de l'étoile saisie
       @return pos_x la position en X de l'astre en question
     */
    public String getAstreX()
    {
	String pos_x = this.formulaire_etoile.getAstreX();
	return pos_x;
    }

    /**
       Permet d'obtenir une chaine représentant la position en Y de l'étoile saisie
       @return pos_y la position en Y de l'astre en question
     */
    public String getAstreY()
    {
	String pos_y = this.formulaire_etoile.getAstreY();
	return pos_y;
    }

    /**
       Permet d'obtenir une chaine représentant l'astre autour duquel tourne le satellite saisie
       @return astre le nom de l'astre référant
     */
    public String getAstreReferant()
    {
	String astre = this.formulaire_satellite.getAstreReferant();
	return astre;
    }

    /**
       Permet d'obtenir une chaine représentant le demi grand axe du satellite saisie
       @return demi_grand_axe le demi grand axe du satellite en question
     */
    public String getAstreDemiGrandAxe()
    {
	String demi_grand_axe = this.formulaire_satellite.getAstreDemiGrandAxe();
	return demi_grand_axe;
    }

    /**
       Permet d'obtenir une chaine représentant le demi petit axe du satellite saisie
       @return demi_petit_axe le demi petit axe du satellite en question
     */
    public String getAstreDemiPetitAxe()
    {
	String demi_petit_axe = this.formulaire_satellite.getAstreDemiPetitAxe();
	return demi_petit_axe;
    }

     /**
       Permet d'obtenir une chaine représentant la période du satellite saisie
       @return periode la période du satellite en question
     */
    public String getAstrePeriode()
    {
	String periode = this.formulaire_satellite.getAstrePeriode();
	return periode;
    }

    /**
       Permet d'effacer les saisies effectuées dans les différents champs des formulaires.
     */
    public void reinitialiserFormulaires()
    {
	this.formulaire_satellite.reinitialiserFormulaire();
	this.formulaire_astre.reinitialiserFormulaire();
	this.formulaire_etoile.reinitialiserFormulaire();
	afficherFormulaireEtoile();
    }
}
