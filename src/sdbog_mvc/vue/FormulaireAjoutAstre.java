package sdbog_mvc.vue;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import javax.swing.*;

/**
   Formulaire permettant la saisie des caractéristiques générales d'un astre.
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
*/
public class FormulaireAjoutAstre extends Formulaire
{
    private JTextField txt_nom_astre;
    private JRadioButton radio_etoile;
    private JRadioButton radio_satellite;
    private ButtonGroup selection_type_astre;

    /**
       Constructeur par défaut.
     */
    public FormulaireAjoutAstre()
    {
	super();

	this.setLayout(new GridLayout(0, 2));
		
	this.add(new JLabel("Nom de l'astre : ", SwingConstants.RIGHT));
	this.txt_nom_astre = new JTextField();
	this.add(this.txt_nom_astre);

	this.add(new JLabel("Type de l'astre : ", SwingConstants.RIGHT));
	this.radio_etoile = new JRadioButton("Étoile");
	this.radio_etoile.setSelected(true);
	this.radio_satellite = new JRadioButton("Satellite");
	
	this.selection_type_astre = new ButtonGroup();
	this.selection_type_astre.add(this.radio_etoile);
	this.selection_type_astre.add(this.radio_satellite);

	JPanel panneau_type_astre = new JPanel(new BorderLayout());
	panneau_type_astre.setBackground(Color.red);
	panneau_type_astre.add(this.radio_etoile, BorderLayout.WEST);
	panneau_type_astre.add(this.radio_satellite, BorderLayout.CENTER);
	this.add(panneau_type_astre);
    }

    /**
       Permet d'ajouter un écouteur lorsque l'utilisateur spécifie
       que l'astre est une étoile.

       @param ecouteur écouteur chargé d'écouter la selection du radio bouton étoile.
    */
    public void ajouterAstreTypeEtoileEcouteur(ActionListener ecouteur)
    {
	this.radio_etoile.addActionListener(ecouteur);
    }

    /**
       Permet d'ajouter un écouteur lorsque l'utilisateur spécifie
       que l'astre est un satellite.

       @param ecouteur écouteur chargé d'écouter la selection du radio bouton satellite.
    */
    public void ajouterAstreTypeSatelliteEcouteur(ActionListener ecouteur)
    {
	this.radio_satellite.addActionListener(ecouteur);
    }

    /**
       Permet de désactiver le radio bouton permettant l'ajout d'un satellite
     */
    public void desactiverSatellite()
    {
	this.radio_satellite.setEnabled(false);
    }

    /**
       Permet d'activer le radio bouton permettant l'ajout d'un satellite
     */
    public void activerSatellite()
    {
	this.radio_satellite.setEnabled(true);
    }

    /**
       Permet de savoir si l'utilisateur à selectionné la case étoile comme type d'astre.
       @return est_une_etoile si la case étoile est selectionnée dans l'interface
       d'ajout d'un astre
    */
    public boolean selectionEtoileUtilisateur()
    {
	boolean est_une_etoile = this.radio_etoile.isSelected();
	return est_une_etoile;
    }

    /**
       Permet d'obtenir le nom de l'astre saisie dans le formulaire.
       @return nom_astre le nom de l'astre en question
    */
    public String getNomAstre()
    {
	String nom_astre = this.txt_nom_astre.getText();

	return nom_astre;
    }

    /**
       Permet d'effacer les saisies effectuées par l'utilisateur
     */
    public void reinitialiserFormulaire()
    {
	this.txt_nom_astre.setText("");
	this.radio_etoile.setSelected(true);
	this.radio_satellite.setSelected(false);
    }
}
