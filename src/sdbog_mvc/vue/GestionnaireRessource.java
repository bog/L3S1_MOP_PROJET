package sdbog_mvc.vue;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import java.io.*;
import java.util.*;
import java.net.URL;
/**
   Classe responsable de l'allocation et du stockage des ressources de l'application
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class GestionnaireRessource
{
    private String chemin;
    private HashMap<String, BufferedImage> images;
    private static GestionnaireRessource instance = null;

    /**
       Permet d'obtenir l'instance courante du gestionnaire de ressource
       @return this.instance l'instance courante du gestionnaire
     */
    public static GestionnaireRessource getInstance()
    {
	if(GestionnaireRessource.instance == null)
	    {
		GestionnaireRessource.instance = new GestionnaireRessource();
	    }
	return GestionnaireRessource.instance;
    }

    /**
       Constructeur par défaut
     */
    private GestionnaireRessource()
    {
	this.chemin = "/img/";
	this.images = new HashMap<String, BufferedImage>();

	chargerImages();
    }

    /**
       Permet de charger toutes les images de l'application
     */
    private void chargerImages()
    {
	try
	    {
		chargerImage("galaxie", "fonds/galaxie.jpg");

		final int nb_etoiles = 5;
		for(int i = 1; i <= nb_etoiles; i++)
		    {
			chargerImage("etoile_" + i, "etoiles/Etoile" + i + ".png");
		    }
		
		chargerImage("Soleil", "etoiles/Etoile4.png");
		
		chargerImage("Alien", "satellites/AlienWorld.png");
		chargerImage("Lune", "satellites/Lune.png");
		chargerImage("Callisto", "satellites/Callisto.png");
		chargerImage("Terre", "satellites/Earth.png");
		chargerImage("Terre2", "satellites/Earth2.png");
		chargerImage("Europa", "satellites/Europa.png");
		chargerImage("Ganymede", "satellites/Ganymede.png");
		chargerImage("Io", "satellites/Io.png");
		chargerImage("Jupiter", "satellites/Jupiter.png");
		chargerImage("Mars", "satellites/Mars.png");
		chargerImage("Mercure", "satellites/Mercury.png");
		chargerImage("Neptune", "satellites/Neptune.png");
		chargerImage("Satellite", "satellites/Satellite.png");
		chargerImage("Saturne", "satellites/Saturne.png");
		chargerImage("Uranus", "satellites/Uranus.png");
		chargerImage("Venus", "satellites/Venus.png");
		

	    }
	catch(Exception e)
	    {
		e.printStackTrace();
	    }
    }

    /**
       Permet de charger une image
       @param nom le nom de l'image à charger
       @param chemin le chemin de l'image à charger
       @throws RessourceDejaExistante si l'images existe déjà en mémoire
       @throws IOException si l'image ne peut être chargée
    */
    private void chargerImage(String nom, String chemin) throws RessourceDejaExistante
								, IOException
    {

	if(this.images.get(nom) != null)
	    {
		throw new RessourceDejaExistante();
	    }
	BufferedImage image;

	InputStream is = getClass().getResourceAsStream(this.chemin + chemin);
	image = ImageIO.read(is);

	this.images.put(nom, image);
    }
    
    /**
       Permet d'obtenir une image de l'application à partir de son nom
       @param nom nom de l'image demandée
       @return image l'image correspondant au nom
       @throws RessourceIntrouvable si aucune image ne correspond au nom spécifié
     */
    public BufferedImage getImage(String nom) throws RessourceIntrouvable
    {
	BufferedImage image;
	try
	    {
		image = this.images.get(nom);
		return image;
	    }
	catch(Exception exception)
	    {
		throw new RessourceIntrouvable();
	    }
	
    }
}
