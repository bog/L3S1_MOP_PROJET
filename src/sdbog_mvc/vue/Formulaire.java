package sdbog_mvc.vue;
import javax.swing.border.*;
import javax.swing.*;

/**
   JPanel contenant un ensemble de composants formant un formulaire.
   
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
*/
public abstract class Formulaire extends JPanel
{
    /**
       Constructeur par défaut.
     */
    public Formulaire()
    {
	this.setBorder(BorderFactory.createRaisedBevelBorder());
    }
}
