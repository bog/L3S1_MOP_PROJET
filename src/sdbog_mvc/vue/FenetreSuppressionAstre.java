package sdbog_mvc.vue;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import javax.swing.*;
import java.util.*;
import sdbog_mvc.modele.Galaxie;
import sdbog_mvc.modele.EnsembleEtoile;
import sdbog_mvc.modele.Astre;

/**
   Fenetre permettant la suppression d'un astre.

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class FenetreSuppressionAstre extends JFrame implements Observer
{
    static final int LARGEUR = 300;
    static final int HAUTEUR = 160;
    static final String TITLE = "Suppression astre";

    private Choice choix_astre;
    private JButton bouton_supprimer;
    
    /**
       Constructeur par défaut.
     */
    public FenetreSuppressionAstre()
    {
	this.getContentPane().setLayout(new GridLayout(0, 1));

	this.choix_astre = new Choice();
	this.getContentPane().add(this.choix_astre);
	
	this.bouton_supprimer = new JButton("Supprimer");
	this.getContentPane().add(this.bouton_supprimer);
	
	this.setSize(FenetreSuppressionAstre.LARGEUR, FenetreSuppressionAstre.HAUTEUR);
	this.setLocationRelativeTo(null);
	this.setTitle(FenetreSuppressionAstre.TITLE);
    }

    /**
       Permet de mettre à jour la liste des astres présent dans un ensemble d'étoile.
       @param observable ensemble d'étoile observée par la fenetre
       @param object non utilisé
     */
    @Override
    public void update(Observable observable, Object object)
    {
	this.choix_astre.removeAll();
	
	EnsembleEtoile etoiles = (EnsembleEtoile)(observable);
	ArrayList<Astre> astres = etoiles.getTousLesAstresTrie();

	for(Astre astre : astres)
	    {
		this.choix_astre.add(astre.getNom());
	    }
    }

    /**
       Permet de lier la validation de la suppression d'un astre  à un écouteur.

       @param ecouteur écouteur chargé d'écouter de la suppression de l'astre
    */
    public void ajouterSupprimerAstreEcouteur(ActionListener ecouteur)
    {
	this.bouton_supprimer.addActionListener(ecouteur);
    }

    /**
       Permet d'obtenir le nom de l'astre à supprimer
       @return nom nom de l'astre à supprimer
     */
    public String getAstreNom()
    {
	String nom = this.choix_astre.getSelectedItem();
	return nom;
    }
}
