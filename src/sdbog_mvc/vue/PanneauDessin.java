package sdbog_mvc.vue;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import java.io.*;
import java.util.*;
import sdbog_mvc.modele.Astre;
import sdbog_mvc.modele.Galaxie;
import sdbog_mvc.modele.Satellite;

/**
   Zone de dessin contenant les astres à dessiner.
   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
 */
public class PanneauDessin extends JPanel implements Observer
{
    private ArrayList<Astre> astres;
    private BufferedImage image_fond;
    
    public PanneauDessin()
    {
	try
	    {
		GestionnaireRessource rsc = GestionnaireRessource.getInstance();
		this.image_fond = rsc.getImage("galaxie");
	    }
	catch(Exception e)
	    {
		e.printStackTrace();
	    }
	
	this.setLayout(null);
	this.astres = null;
	this.setBackground(Color.white);
    }

    @Override
    public void paintComponent(Graphics graphic)
    {
	graphic.drawImage( this.image_fond
			   , 0
			   , 0
			   , FenetrePrincipale.LARGEUR
			   , FenetrePrincipale.HAUTEUR 
			   , null);
	
    	///graphic.setColor(Color.black);
	
	if( this.astres != null )
	    {
		for(Astre astre : this.astres)
		    {
			int taille = 128;
			
			try
			    {
				Satellite s = (Satellite)astre;
				int profondeur = s.getProfondeur() + 1;
				
				if( profondeur > 0 )
				    {
					taille /= profondeur;
				    }
				
				graphic.setColor(Color.black);
			    }
			catch(Exception e)
			    {
				graphic.setColor(Color.yellow);
			    }
			
			// graphic.fillOval((int)astre.getPosition().getX()
			// 		 , (int)astre.getPosition().getY()
			// 		 , taille
			// 		 , taille);

			BufferedImage image;

			try
			    {
				String nom = astre.getNom();
				image = GestionnaireRessource.getInstance().getImage(nom);
			    }
			catch(Exception e)
			    {
				image = null;
			    }

			if(image == null)
			    {
				try
				    {
					image = GestionnaireRessource.getInstance().getImage("Alien");
				    }
				catch(Exception ee)
				    {
					image = null;
					ee.printStackTrace();
				    }
			    }
			
			graphic.drawImage(image
					   , (int)astre.getPosition().getX() - taille/2
					   , (int)astre.getPosition().getY() - taille/2
					   , taille
					   , taille
					   , null);

			graphic.setColor(Color.black);
			
			graphic.drawString(astre.getNom()
					   , (int)astre.getPosition().getX() - taille/2
					   , (int)astre.getPosition().getY() - taille/2 - taille/12);
		    }
	    }
    }
    
    /**
       Permet de mettre à jour la position des astres.
       @param observable Galaxie observée par la fenetre
       @param object non utilisé
    */
    public void update(Observable observable, Object object)
    {
	Galaxie galaxie = (Galaxie)(observable);
	this.astres = galaxie.getTousLesAstres();
	repaint();
    }
}
