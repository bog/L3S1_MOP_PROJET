package sdbog_mvc.vue;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.util.*;
import javax.imageio.ImageIO;
import java.io.*;

/**
   Fenetre principale de l'application Carte stellaire.

   @author Bérenger Ossete Gombe
   @author Sylvain Dumontet
*/
public class FenetrePrincipale extends JFrame
{
    public final static int LARGEUR = 1024;
    public final static int HAUTEUR = 780;
    public final static String TITLE = "Go-go-go-gorille haltérophile !";

    private PanneauDessin panneau_dessin;
    
    private JMenuBar barreDeMenu;
    private JMenuItem menu_ajouter_astre;
    private JMenuItem menu_supprimer_astre;
    private JMenuItem menu_nouveau_systeme;
    private JMenuItem menu_enregistrer_systeme;
    private JMenuItem menu_charger_systeme;

    /**
       Constructeur par défault.
     */
    public FenetrePrincipale()
    {
	ajouterBarreDeMenu();
	
	this.panneau_dessin = new PanneauDessin();
	this.setContentPane(this.panneau_dessin);
	
	this.setSize(FenetrePrincipale.LARGEUR, FenetrePrincipale.HAUTEUR);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setLocationRelativeTo(null);
	this.setTitle(FenetrePrincipale.TITLE);
	this.setVisible(true);
    }

    /**
       Ajoute la barre de menu principale.
     */
    private void ajouterBarreDeMenu()
    {
	this.barreDeMenu = new JMenuBar();
	
	JMenu menu_fichier = new JMenu("Fichier");
        this.menu_nouveau_systeme = new JMenuItem("Nouveau Système");
	menu_fichier.add(this.menu_nouveau_systeme);
	
	this.menu_enregistrer_systeme =  new JMenuItem("Enregistrer un système");
	menu_fichier.add(this.menu_enregistrer_systeme);

	this.menu_charger_systeme =  new JMenuItem("Charger un système");
	menu_fichier.add(this.menu_charger_systeme);
	
	JMenuItem menu_item_quitter = new JMenuItem("Quitter");
	menu_item_quitter.addActionListener(new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent event)
		{
		    System.exit(0);
		}
	    });
	menu_fichier.add(menu_item_quitter);
	this.barreDeMenu.add(menu_fichier);
	
	JMenu menu_edition = new JMenu("Édition");
	this.menu_ajouter_astre = new JMenuItem("Ajouter un astre");
	menu_edition.add(this.menu_ajouter_astre);

	this.menu_supprimer_astre = new JMenuItem("Supprimer un astre");
	menu_edition.add(this.menu_supprimer_astre);
	
	this.barreDeMenu.add(menu_edition);
	
	this.setJMenuBar(this.barreDeMenu);
    }
    
    /**
       Permet d'ajouter un écouteur au menu permettant d'ajouter un astre.
       @param action_listener ActionListener écoutant l'ajout d'un astre
     */
    public void setAjouterAstreEcouteur(ActionListener action_listener)
    {
	this.menu_ajouter_astre.addActionListener(action_listener);
    }

    /**
       Permet d'ajouter un écouteur au menu permettant de supprimer un astre.
       @param action_listener ActionListener écoutant la suppression d'un astre
     */
    public void setSupprimerAstreEcouteur(ActionListener action_listener)
    {
	this.menu_supprimer_astre.addActionListener(action_listener);
    }

    /**
       Permet d'ajouter un écouteur au menu permettant de créer un nouveau système.
       @param action_listener ActionListener écoutant la création d'un système
    */
    public void setNouveauSystemeEcouteur(ActionListener action_listener)
    {
	this.menu_nouveau_systeme.addActionListener(action_listener);
    }
    
    /**
       Permet d'ajouter un écouteur au menu permettant d'enregistrer un système.
       @param action_listener ActionListener écoutant l'enregistrement d'un système
    */
    public void setEnregistrerSystemeEcouteur(ActionListener action_listener)
    {
	this.menu_enregistrer_systeme.addActionListener(action_listener);
    }

    /**
       Permet d'ajouter un écouteur au menu permettant de charger un système.
       @param action_listener ActionListener écoutant le chargement d'un système
    */
    public void setChargerSystemeEcouteur(ActionListener action_listener)
    {
	this.menu_charger_systeme.addActionListener(action_listener);
    }


    /**
       Permet de définir la galaxie observée par la fenetre principale.
       @param galaxie galaxie à observer
     */
    public void setObservable(Observable galaxie)
    {	
	galaxie.addObserver(this.panneau_dessin);
    }
}
